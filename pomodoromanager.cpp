#include "pomodoromanager.h"

PomodoroManager::PomodoroManager() :
	PluginBase(this),
	m_status(IPomodoroManager::TimerStatus::PAUSE)
{
	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IPomodoroManager), this},
	},
	{
		{INTERFACE(INotificationManager), m_notificationManager},
		{INTERFACE(IPomodoroSettingsData), m_settings},
		{INTERFACE(IUserTaskDataExtention), m_userTask},
		{INTERFACE(IUserTaskPomodoroDataExtention), m_userTaskPomodoro},
	});
	connect(&periodsTimer, &QTimer::timeout, this, &PomodoroManager::OnTimerTick);
}

PomodoroManager::~PomodoroManager()
{
}

void PomodoroManager::SetActiveProject(QModelIndex index)
{
	currentTask = index;
}

QPointer<IExtendableDataModel> PomodoroManager::GetTaskModel()
{
	return m_userTask->getModel();
}

QPointer<IExtendableDataModelFilter> PomodoroManager::GetTaskModelFilter()
{
	m_userTaskFilter = m_userTask->getModel()->getFilter();
	m_userTaskFilter->setColumns({
		{INTERFACE(IUserTaskDataExtention), {"name"}},
		{INTERFACE(IUserTaskPomodoroDataExtention), {"pomodorosFinished"}}
	});
	return m_userTaskFilter;
}

void PomodoroManager::changeStatus(IPomodoroManager::TimerStatus status)
{
	if(m_status == status)
		return;

	switch(status)
	{
	case IPomodoroManager::TimerStatus::REST:
		//		notificationTimerId = m_notificationManager->SetAlarm(INotificationManager::RTC_TIME,
		//		                QDateTime::currentDateTime().addSecs(m_settings->easyRestDuration()));

		//		m_notificationManager->SetAlarmedNotification(INotificationManager::RTC_TIME,
		//		        QDateTime::currentDateTime().addSecs(m_settings->easyRestDuration()),
		//		        "Message from Pomodoro:",
		//		        "Let's go back to work");
		timeLeft = timeLeft.fromMSecsSinceStartOfDay(m_settings->easyRestDuration() * 1000);
		periodsTimer.start(1000);
		break;
	case IPomodoroManager::TimerStatus::WORK:
		//		notificationTimerId = m_notificationManager->SetAlarm(INotificationManager::RTC_TIME,
		//		                QDateTime::currentDateTime().addSecs(m_settings->workSessionDuration()));

		//		m_notificationManager->SetAlarmedNotification(INotificationManager::RTC_TIME,
		//		        QDateTime::currentDateTime().addSecs(m_settings->workSessionDuration()),
		//		        "Message from Pomodoro:",
		//		        "You can take a rest now");
		timeLeft = timeLeft.fromMSecsSinceStartOfDay(m_settings->workSessionDuration() * 1000);
		periodsTimer.start(1000);
		break;
	case IPomodoroManager::TimerStatus::PAUSE:
		break;
	}

	m_status = status;
	emit onStatusChanged(m_status, timeLeft);
}

void PomodoroManager::onReady()
{
	emit onStatusChanged(IPomodoroManager::TimerStatus::PAUSE, timeLeft);
}

void PomodoroManager::OnTimerTick()
{
	if(timeLeft.msecsSinceStartOfDay())
	{
		timeLeft = timeLeft.addSecs(-1);
		emit onTimerTick(timeLeft);
	}
	else
	{
		OnTimerEnded(notificationTimerId);
	}
}

void PomodoroManager::OnTimerEnded(int timerId)
{
	if(notificationTimerId != timerId) return;
	if(!currentTask.isValid()) return;

	periodsTimer.stop();

	switch(m_status)
	{
	case IPomodoroManager::TimerStatus::REST:
		changeStatus(IPomodoroManager::TimerStatus::PAUSE);
		break;

	case IPomodoroManager::TimerStatus::WORK:
	{
		auto branchIndex = currentTask;
		while(branchIndex.isValid())
		{
			auto data = m_userTask->getModel()->getItem(branchIndex);
			auto& value = data[INTERFACE(IUserTaskPomodoroDataExtention)]["pomodorosFinished"];
			value = value.toInt() + 1;
			m_userTask->getModel()->updateItem(branchIndex, data);
			branchIndex = branchIndex.parent();
		}
		changeStatus(IPomodoroManager::TimerStatus::REST);
	}
	break;

	case IPomodoroManager::TimerStatus::PAUSE:
		return;
	}
}

QModelIndex PomodoroManager::GetActiveProject()
{
	return currentTask;
}

QPair<QString, quint16> PomodoroManager::getActiveProjectPomodoros()
{
	if(!currentTask.isValid())
		return QPair<QString, quint16>();
	auto data = m_userTask->getModel()->getItem(currentTask);
	return QPair<QString, quint16>(
	                data[INTERFACE(IUserTaskDataExtention)]["name"].toString(),
	                data[INTERFACE(IUserTaskPomodoroDataExtention)]["pomodorosFinished"].toUInt());
}

QVariantMap PomodoroManager::getSettings()
{
	return {};
}

void PomodoroManager::setSetting(QString setting, QVariant value)
{

}

IPomodoroManager::TimerStatus PomodoroManager::getStatus()
{
	return m_status;
}
